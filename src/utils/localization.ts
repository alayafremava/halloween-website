import locales_en from "../translations/en.json";
import locales_ar from "../translations/ar.json";

const MESSAGES = {
  en: locales_en,
  ar: locales_ar,
};

const LOCALES = {
  EN: "en",
  AR: "ar",
};

const FONT_STYLES = {
  EN: '"Rubik", Arial, sans-serif',
  AR: '"Noto Naskh Arabic", serif',
};

let currentLocale: (typeof LOCALES)[keyof typeof LOCALES] = "en";

export function changeLocale() {
  currentLocale = currentLocale === LOCALES.EN ? LOCALES.AR : LOCALES.EN;

  document.body.style.fontFamily = FONT_STYLES[currentLocale];

  const header = document.querySelector("header");
  header.classList.toggle("ar", currentLocale === LOCALES.AR);

  translateText();
}

function translateText() {
  const localeMessages = MESSAGES[currentLocale];

  const elemensForTranslation = document.querySelectorAll("[data-i18n]");

  elemensForTranslation.forEach((element) => {
    const key = element.getAttribute("data-i18n");

    element.innerHTML = localeMessages[key];
  });
}
