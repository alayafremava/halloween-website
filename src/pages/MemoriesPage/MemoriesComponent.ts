import "./styles.css";

import image1 from "../../assets/images/memories/image01.png";
import image2 from "../../assets/images/memories/image02.png";
import image3 from "../../assets/images/memories/image03.png";
import image4 from "../../assets/images/memories/image04.png";
import image5 from "../../assets/images/memories/image05.png";
import image6 from "../../assets/images/memories/image06.png";
import image7 from "../../assets/images/memories/image07.png";
import image8 from "../../assets/images/memories/image08.png";
import image9 from "../../assets/images/memories/image09.png";
import image10 from "../../assets/images/memories/image10.png";

export function MemoriesComponent(): HTMLElement {
  const host = document.createElement("section");
  host.classList.add("gallery", "section");
  host.setAttribute("id", "gallery");

  const container = document.createElement("div");
  container.classList.add("container");

  const heading = document.createElement("h2");
  heading.classList.add("memories-section-heading");
  heading.setAttribute("data-i18n", "halloween-memories");
  heading.textContent = "Halloween Memories";
  heading.dataset.i18n = "section.memories.title";
  container.appendChild(heading);

  const galleryContainer = document.createElement("div");
  galleryContainer.classList.add("gallery-container");
  container.appendChild(galleryContainer);

  const images = [
    image1,
    image2,
    image3,
    image4,
    image5,
    image6,
    image7,
    image8,
    image9,
    image10,
  ];

  images.forEach((imageSrc, index) => {
    const imageElement = document.createElement("img");
    imageElement.src = imageSrc;
    imageElement.alt = `Image ${index + 1}`;
    imageElement.classList.add("gallery-image");
    galleryContainer.appendChild(imageElement);
  });

  host.append(container);

  return host;
}
