import "./styles.css";

export function HeroComponent(): HTMLElement {
  const section = document.createElement("section");
  section.classList.add("hero", "section");

  const time = document.createElement("time");
  time.classList.add("date");
  time.dataset.i18n = "section.hero.date";
  time.innerText = "31 October 2023";

  const title = document.createElement("h1");
  title.classList.add("hero__title");
  title.dataset.i18n = "section.hero.title";
  title.innerText = "It's Halloween Party O'Clock!";

  const btn = document.createElement("div");
  btn.classList.add("hero__btn");

  section.append(time);
  section.append(title);
  section.append(btn);

  return section;
}
