import {
  renderTicketOptions,
  ticketOptions,
} from "../../shared/TicketOptions/TicketOptions";

export function HostPage(): HTMLElement {
  const section = document.createElement("section-tickets");
  section.classList.add("section-tickets");

  const title = document.createElement("h1");
  title.classList.add("setion__title");

  const tickets = renderTicketOptions(ticketOptions);

  section.appendChild(tickets);
  return section;
}
