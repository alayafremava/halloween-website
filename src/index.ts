import { HeroComponent } from "./pages/HeroPage/HeroPage";
import "./shared/Header/Header";
import { Header } from "./shared/Header/Header";
import { MemoriesComponent } from "./pages/MemoriesPage/MemoriesComponent";
import "./styles.css";
import { Footer } from "./shared/Footer/Footer";
import { HostPage } from "./pages/HostsPage/HostPage";

document.addEventListener("DOMContentLoaded", () => {
  const app = document.querySelector("#app");
  if (!app) {
    return; // Exit early if #app is not found
  }

  const components = [
    Header(),
    HeroComponent(),
    MemoriesComponent(),
    HostPage(),
    Footer(),
  ];

  components.forEach((component) => app.appendChild(component));
});
