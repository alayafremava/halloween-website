import "./styles.css";

export function Button(label = "Botton", i18n = "", isActive = false) {
  const button = document.createElement("button");
  button.className = isActive ? "app-btn--active" : "";
  button.classList.add("app-btn");

  button.innerText = label;
  button.dataset.i18n = i18n;

  return button;
}
