import { changeLocale } from "../../utils/localization";
import { Button } from "../Button/Button";
import "./styles.css";

const headerContent = `
        <div class="logo">
          <a class="logo__link" href="#" data-i18n="header.logo">Party Time!</a>
        </div>
        <nav class="nav">
          <ul class="nav__list">
            <li class="nav__item">
              <a class="nav__item-link" href="#" data-i18n="header.nav.home">Home</a>
            </li>
            <li class="nav__item">
              <a class="nav__item-link" href="#" data-i18n="header.nav.gallery">Gallery</a>
            </li>
            <li class="nav__item">
              <a class="nav__item-link" href="#" data-i18n="header.nav.aboutParty">About Party</a>
            </li>
            <li class="nav__item">
              <a class="nav__item-link" href="#" data-i18n="header.nav.reservation">Reservation</a>
            </li>
            <li class="nav__item">
              <a class="nav__item-link" href="#" data-i18n="header.nav.contacts">Contacts</a>
            </li>
          </ul>
        </nav>
        <div class="translate"><a class="translate__link" href="#" id="change-locale">EN عرب</a></div>
        <div class="reservation"></div>
  `;

export const Header = () => {
  const header = document.createElement("header");
  header.className = "header";
  header.innerHTML = headerContent;

  // localization
  header
    .querySelector("#change-locale")
    .addEventListener("click", changeLocale);

  header.appendChild(Button("Reservation","header.btn.reservation"));

  return header;
};
