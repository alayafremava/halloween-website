import "./ticketOptions.css";
import { Button } from "../Button/Button";

type Feature = {
  title: string;
  titleI18N: string;
};

export type TicketOption = {
  id: string;
  title: string;
  titleI18N: string;
  price: number;
  features: Feature[];
};

export const ticketOptions: TicketOption[] = [
  {
    id: "1",
    title: "Single Ticket",
    titleI18N: "section.hosts.card.left.title",
    price: 100,
    features: [
      {
        title: "Blood Drink",
        titleI18N: "section.hosts.card.left.drink",
      },
      {
        title: "Haunted House Tour",
        titleI18N: "section.hosts.card.left.activity",
      },
      {
        title: "Horror Movie Marathon",
        titleI18N: "section.hosts.card.left.addition",
      },
    ],
  },
  {
    id: "2",
    title: "Couple Ticket",
    titleI18N: "section.hosts.card.center.title",
    price: 150,
    features: [
      {
        title: "Blood Drink",
        titleI18N: "section.hosts.card.center.drink",
      },
      {
        title: "Horror Movie Marathon",
        titleI18N: "section.hosts.card.center.activity",
      },
      {
        title: "Pumpkin Carving Contest",
        titleI18N: "section.hosts.card.center.addition",
      },
    ],
  },
  {
    id: "3",
    title: "Combo Ticket",
    titleI18N: "",
    price: 300,
    features: [
      {
        title: "Blood Drink",
        titleI18N: "section.hosts.card.right.drink",
      },
      {
        title: "Haunted House Tour",
        titleI18N: "section.hosts.card.right.activity",
      },
      {
        title: "Horror Movie Marathon",
        titleI18N: "section.hosts.card.right.addition",
      },
    ],
  },
];

// Функция для отображения вариантов билетов на веб-странице
export function renderTicketOptions(
  ticketOptions: TicketOption[],
): HTMLElement {
  const container = document.createElement("div");
  container.classList.add("ticket-container");
  const header = document.createElement("div");
  header.classList.add("ticket-header");
  header.textContent = "Let's be your hosts";
  header.dataset.i18n = "section.hosts.title";

  container.append(header);

  ticketOptions.forEach((ticket) => {
    const ticketElement = document.createElement("div");
    ticketElement.id = `ticket-${ticket.id}`;
    ticketElement.className = "ticket";
    ticketElement.innerHTML = `
          <h2 data-i18n=${ticket.titleI18N}>${ticket.title}</h2>
          <p class="price">$${ticket.price}</p>
          <ul>
            ${ticket.features
              .map(
                (feature) =>
                  `<li data-i18n=${feature.titleI18N}>${feature.title}</li>`,
              )
              .join("")}
          </ul>
        `;

    const button = Button(
      "Buy Ticket",
      "section.hosts.card.btn",
      ticket.id === "2",
    );
    ticketElement.appendChild(button);

    container.append(ticketElement);
  });

  return container;
}
