import "./footer.css";

const footerContent = `
<div class="footer-container">
    <div class="bat-img"></div>
    <div class="footer-contact-info">
      <p data-i18n="section.footer.title">PHONE RESERVATION? (+1) 987 46 52</p>
    </div>
    <div class="social-media">
      <a href="#" class="social-media-link">
      <img class="social-media-img" src="./src/assets/icons/footer/behance.svg" alt="Be" /></a>
      <a href="#" class="social-media-link">
      <img class="social-media-img" src="./src/assets/icons/footer/figma.svg" alt="Figma" /></a>
      <a href="#" class="social-media-link">
      <img class="social-media-img" src="./src/assets/icons/footer/linkedIn.svg" alt="LinkedIn" /></a>
      <a href="#" class="social-media-link">
      <img class="social-media-img" src="./src/assets/icons/footer/instagram.svg" alt="Instagram" /></a>
      <a href="#" class="social-media-link">
      <img class="social-media-img" src="./src/assets/icons/footer/youtube.svg" alt="YouTube" /></a>
    </div>
  </div>
`;

export const Footer = (): HTMLElement => {
  const section = document.createElement("section");
  section.classList.add("section", "footer-section");
  section.innerHTML = footerContent;
  return section;
};
